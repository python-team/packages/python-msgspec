This package is maintained with git-buildpackage(1). It follows DEP-14 for
branch naming (e.g. using debian/master for the current version in Debian
unstable due Debian Python team policy).

It uses pristine-tar(1) to store enough information in git to generate bit
identical tarballs when building the package without having downloaded an
upstream tarball first.

When working with patches it is recommended to use "gbp pq import" to import
the patches, modify the source and then use "gbp pq export --commit" to commit
the modifications.

The changelog is generated using "gbp dch" so if you submit any changes don't
bother to add changelog entries but rather provide a nice git commit message
that can then end up in the changelog.

It is recommended to build the package with pbuilder using:

    gbp buildpackage --git-pbuilder

For information on how to set up a pbuilder environment see the git-pbuilder(1)
manpage. In short:

    DIST=sid git-pbuilder create
    gbp clone https://salsa.debian.org/python-team/packages/python-msgspec.git
    cd python-msgspec
    gbp buildpackage --git-pbuilder


Javascript files in debian/js
-----------------------------
While writing Upstream is using the libraries vega v5.22.1, vega-lite v5.5.0
and vega-embed v6.21.0 to create some graphics within the documentation. We
can use the package libjs-vega to replace the usage of Javascript library
pulled in from the NPM CDN.

For the libraries of vega-embed and vega-lite Debian hasn't libraries we
could use for pointing to them. Instead the minimized and non minimized
versions of them is included within the packaging.

vega-embed
..........
The files vega-embed*.js did get picked from the tarball of version 6.21.0
https://registry.npmjs.org/vega-embed/-/vega-embed-6.21.0.tgz
and placed into the folder debian/js.

vega-lite
.........
The files vega-lite*.js did get picked from the tarball of version 6.21.0
https://registry.npmjs.org/vega-lite/-/vega-lite-5.5.0.tgz
and placed into the folder debian/js.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Thu, 25 Jul 2024 13:21:00 +0700
