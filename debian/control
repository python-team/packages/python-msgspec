Source: python-msgspec
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Stein Magnus Jodal <jodal@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 libpython3-all-dev,
 pybuild-plugin-pyproject,
 python3-all,
 python3-pytest <!nocheck>,
Build-Depends-Indep:
 furo <!nodoc>,
 python3-setuptools,
 python3-sphinx <!nodoc>,
 python3-sphinx-copybutton <!nodoc>,
 python3-sphinx-design <!nodoc>,
 python3-ipython <!nodoc>,
 python-attr-doc <!nodoc>,
 python3-doc <!nodoc>,
Standards-Version: 4.7.0
Testsuite: autopkgtest-pkg-pybuild
Rules-Requires-Root: no
Homepage: https://github.com/jcrist/msgspec
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-msgspec
Vcs-Git: https://salsa.debian.org/python-team/packages/python-msgspec.git

Package: python-msgspec-doc
Section: doc
Architecture: all
Depends:
 libjs-vega,
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: Fast serialization/validation Python library (Documentation)
 msgspec is a library that provides the following features for serialization
 and validation of JSON, MessagePack, YAML, and TOML data:
 .
  * High performance encoders/decoders for common protocols. The JSON and
    MessagePack implementations regularly benchmark as the fastest options
    for Python.
  * Support for a wide variety of Python types. Additional types may be
    supported through extensions.
  * Zero-cost schema validation using familiar Python type annotations.
    In benchmarks msgspec decodes and validates JSON faster than orjson can
    decode it alone.
  * A speedy Struct type for representing structured data. If you already
    use dataclasses or attrs, structs should feel familiar. However, they're
    5-60x faster for common operations.
 .
 This package contains the documentation for python3-msgspec.

Package: python3-msgspec
Architecture: amd64 arm64 mips64el ppc64el riscv64 alpha hurd-amd64 loong64
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Suggests:
 python-msgspec-doc,
 python3-tomli-w,
 python3-yaml,
Description: Fast serialization/validation Python library (Python 3)
 msgspec is a library that provides the following features for serialization
 and validation of JSON, MessagePack, YAML, and TOML data:
 .
  * High performance encoders/decoders for common protocols. The JSON and
    MessagePack implementations regularly benchmark as the fastest options
    for Python.
  * Support for a wide variety of Python types. Additional types may be
    supported through extensions.
  * Zero-cost schema validation using familiar Python type annotations.
    In benchmarks msgspec decodes and validates JSON faster than orjson can
    decode it alone.
  * A speedy Struct type for representing structured data. If you already
    use dataclasses or attrs, structs should feel familiar. However, they're
    5-60x faster for common operations.
 .
 This package contains the Python 3 version.
